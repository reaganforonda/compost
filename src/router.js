import React from 'react';
import {Switch, Route} from 'react-router-dom';

import landing from './Components/Landing/Landing'
import Login from './Components/Login/Login'
import Register from './Components/Register/Register'
import UserDashboard from './Components/UserDashboard/UserDashboard';

export default function Routes(props){
    return (
        <Switch>
            <Route exact path='/' component={landing}/>
            <Route path='/login' component={Login}/>
            <Route path='/register' component={Register}/>
            <Route path='/dashboard' component={UserDashboard}/>
        </Switch>
    )
}
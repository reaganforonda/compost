import React from 'react';
import axios from 'axios';
import * as generalUtilities from  '../../utilities/generalUtilities'

export default class Login extends React.Component{
    constructor(props){
        super(props);

        this.state={
            email: '',
            pw: '',
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.resetState = this.resetState.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleInputChange(e){
        this.setState({[e.target.name]: e.target.value});
    }

    resetState(){
        this.setState({
            email: '',
            pw:''
        })
    }

    handleLogin(e){
        e.preventDefault();
        
        let user = {
            email: this.state.email.toLowerCase(),
            pw: this.state.pw
        }

        console.log(user);
        axios.post('/api/auth/login', user).then(result=> {
            console.log(result);
            this.props.history.push('/dashboard')
        })

    }

    render(){
        return (
            <div className='login-container'>
                <section className='login-container'>
                    <div className='login-input-row'>
                        <input name='email' type='email' onChange={(e)=>this.handleInputChange(e)} required placeholder='Email'/>
                    </div>
                    <div className='login-input-row'>
                        <input name='pw' type='password' onChange={(e)=>this.handleInputChange(e)} required placeholder='Password'/> 
                    </div>
                    <div className='login-input-row'>
                        <input onClick={(e)=> this.handleLogin(e)} name='submit' type='submit' placeholder='Submit'/>
                    </div>
                    <div className='login-input-row'>
                        New to Compost?  Please Register
                    </div>
                </section>
            </div>
        )
    }
}
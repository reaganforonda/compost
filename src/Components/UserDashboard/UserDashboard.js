import React from 'react';
import Header from './DashboardHeader';
import {withRouter} from 'react-router'
import axios from 'axios';

export class UserDashboard extends React.Component{
    constructor(props){
        super(props);

        this.state={
            user_id: null
        }
    }


    componentDidMount(){
        axios.get('/api/auth/me').then((user) => {
            this.setState({user_id: user.data.user_id})
        }).catch((error) => {
            console.log(`Error: ${error.response.status}`)
            this.props.history.push('/login')
        })
    }

    render(){
        return (
            <div className='dashboard-container'>
                <Header/>
                <div>
                    Dashboard
                </div>
            </div>
        )
    }
}

export default withRouter(UserDashboard);


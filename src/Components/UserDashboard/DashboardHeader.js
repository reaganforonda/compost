import React from 'react';

export default class DashboardHeader extends React.Component{
    render(){
        return (
            <div className='header-container'>
                <div className='nav-content'>
                    <div>
                        Logo
                    </div>
                    <div className='nav-profile-container'>
                        <p>Shopping Cart-PlaceHolder</p>
                        <p>User Profile Placeholder</p>
                    </div>
                </div>
            </div>
        )
    }
}
import React from 'react';
import axios from 'axios';
import '../../Assets/Styles/register.css';
import * as generalUtilities from  '../../utilities/generalUtilities';
import {withRouter} from 'react-router'

export class Register extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            firstName:'',
            lastName: '',
            email: '',
            pw:'',
            confirmPw: '',
            phone: '',
            zipcode: '',
            pwErrorClass: 'form-input-row-error-hidden',
            emailErrorClass: 'form-input-row-error-hidden',
            zipCodeErrorClass: 'form-input-row-error-hidden'
        }
        this.handleInput = this.handleInput.bind(this);
        this.validateInformation = this.validateInformation.bind(this);
        this.resetState = this.resetState.bind(this);
        this.handleSubmitForm = this.handleSubmitForm.bind(this);
    }

    handleInput(e){
        this.setState({[e.target.name]: e.target.value})
    }

    validateInformation(){

        if(this.state.pw !== this.state.confirmPw){
            this.setState({pwErrorClass: 'form-input-row-error'})
            if(!generalUtilities.validateEmail(this.state.email)){
                this.setState({emailErrorClass: 'form-input-row-error'})
                if(!generalUtilities.validateZipCode(this.state.zipcode)){
                    this.setState({zipCodeErrorClass: 'form-input-row-error'})
                    return false;
                }
                return false;
            }    
            return false;
        }
        if(!generalUtilities.validateEmail(this.state.email)){
            this.setState({emailErrorClass: 'form-input-row-error'})
            if(this.state.pw !== this.state.confirmPw){
                if(!generalUtilities.validateZipCode(this.state.zipcode)){
                    this.setState({zipCodeErrorClass: 'form-input-row-error'})
                    return false;
                }
                return false;        
            }
            return false;
        }
        if(!generalUtilities.validateZipCode(this.state.zipcode)){
            this.setState({zipCodeErrorClass: 'form-input-row-error'})
            if(!generalUtilities.validateEmail(this.state.email)){
                this.setState({emailErrorClass: 'form-input-row-error'})
                if(this.state.pw !== this.state.confirmPw){
                    this.setState({pwErrorClass: 'form-input-row-error'})
                    return false;
                }
                return false;
            }
            return false;
        }
        return true;
    }

    resetState(){
        this.setState({
            firstName:'',
            lastName: '',
            email: '',
            pw:'',
            confirmPw: '',
            phone: '',
            zipcode: '',
            pwErrorClass: 'form-input-row-error-hidden',
            emailErrorClass: 'form-input-row-error-hidden',
            zipCodeErrorClass: 'form-input-row-error-hidden'
        })
    }

    handleSubmitForm(e){
        e.preventDefault(e);

        let user={
            firstName:this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            pw: this.state.pw,
            phone: this.state.phone,
            zipcode: this.state.zipcode,
        }

        if(this.state.pw === '') {
            alert('Password Required');
        }
        if(this.validateInformation()){
            axios.post('/api/auth/register', user).then(result=>{
                console.log(result);
            }).catch((err) => {
                console.log(err);
            })

            this.resetState();
        }
    }

    render(){
        return (
            <div className='register-container'>
                <section className='register-header-container'>
                    <form className='register-form'>
                        <h2>Please Create An Account To Get Started</h2>
                        <div className='form-input-row'>
                            <input onChange={(e) => this.handleInput(e)} type='text' 
                                required='true' name='firstName' placeholder='First Name'/>
                        </div>
                        <div className='form-input-row'>
                            <input name='lastName' placeholder='Last Name'
                                required='true' onChange={(e) => this.handleInput(e)}/>
                        </div>
                        <div className='form-input-row'>
                            <input name='email' placeholder='Email Address'
                                type='email' required='true' onChange={(e) => this.handleInput(e)}/>
                        </div>
                        <div className={this.state.emailErrorClass}>
                            <p>Please Enter A Valid Email</p>
                        </div>
                        <div className='form-input-row'>                    
                            <input name='pw' placeholder='Password' 
                                type='password' required
                                onChange={(e) => this.handleInput(e)}/>
                        </div>
                        <div className='form-input-row'>                    
                            <input name='confirmPw' placeholder='Confirm Password' 
                                required type='password' 
                                onChange={(e) => this.handleInput(e)}/>
                        </div>
                        <div className={this.state.pwErrorClass}>
                            <p>Passwords Do Not Match</p>
                        </div>
                        <div className='form-input-row'>
                            <input name='phone' placeholder='Phone' 
                                required onChange={(e) => this.handleInput(e)}/>
                        </div>
                        <div className='form-input-row'>
                            <input name='zipcode' placeholder='Zipcode' 
                                required onChange={(e) => this.handleInput(e)}/>
                        </div>
                        <div className={this.state.zipCodeErrorClass}>
                            <p>Please Enter a Valid Zipcode</p>
                        </div>
                        <div className='form-input-row'>
                            <input onClick={(e)=> this.handleSubmitForm(e)} className='registration-submit-btn' 
                                type='submit' placeholder='Register'/>
                        </div>
                    </form>
                </section>
            </div>
        )
    }
}

export default withRouter(Register);
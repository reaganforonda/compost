import React from 'react';
import styled from 'styled-components';
import '../../Assets/Styles/landing.css'


export default class Landing extends React.Component{

    render(){
        
        const Header = styled.div`
            font-size: 3em;
            color: yellow;
        `

        return(
          <div className='landing-container'>
            <nav className='landing-header-bar'>
                <div className='logo-holder'>
                    Logo
                </div>
                
                <div className='landing-header-links'>
                    <div>
                        Register
                    </div>
                    <div>
                        Login
                    </div>
                </div>

            </nav>
          </div>
        )
    }
}
import React from 'react';
import ReactDOM from 'react-dom';
import './Assets/Styles/reset.css'
import App from './App';
// import registerServiceWorker from './registerServiceWorker';
import { unregister } from "./registerServiceWorker";
import { Provider } from "react-redux";
import { HashRouter } from "react-router-dom";
import Store from './ducks/store';

ReactDOM.render(<Provider store={Store}><HashRouter><App /></HashRouter></Provider>, document.getElementById('root'));
// registerServiceWorker();
unregister();

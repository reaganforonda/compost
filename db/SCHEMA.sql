DROP TABLE IF EXISTS users;

CREATE TABLE users(
    user_id SERIAL PRIMARY KEY,
    first_name VARCHAR(45),
    last_name VARCHAR(45),
    email TEXT,
    pw TEXT,
    phone VARCHAR(20),
    zipcode INTEGER,
    address VARCHAR(45),
    city VARCHAR(45),
    state VARCHAR(45)
)
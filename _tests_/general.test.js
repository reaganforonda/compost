let generalUtilityFunc = require('../src/utilities/generalUtilities');

/* 
 * Test validateZipcode Function
 * Should Return Boolean if valid or not
 */
test('Should return False', ()=> {
    let testValue = 'jjnksdnflsdnfsd';
    let testResult = generalUtilityFunc.validateZipCode(testValue);

    expect(testResult).toBeFalsy();
});

test('Should return False', ()=> {
    let testValue = '9';
    let testResult = generalUtilityFunc.validateZipCode(testValue);

    expect(testResult).toBeFalsy();
});

test('Should return False', ()=> {
    let testValue = '894589';
    let testResult = generalUtilityFunc.validateZipCode(testValue);

    expect(testResult).toBeFalsy();
});

test('Should return True', ()=> {
    let testValue = '90201';
    let testResult = generalUtilityFunc.validateZipCode(testValue);

    expect(testResult).toBeTruthy();
});

/* 
 * Test validateEmail Function
 * Should Return Boolean if valid or not
 */
test('If invalid email - should return false', ()=> {
    let testEmail = 'jdkjf';

    let result = generalUtilityFunc.validateEmail(testEmail);
    expect(result).toBeFalsy();
})

test('If invalid emial - should return false', ()=> {
    let testEmail = 'xnkdjl@'

    let result = generalUtilityFunc.validateEmail(testEmail);
    expect(result).toBeFalsy();
})

test('If invalid emial - should return false', ()=> {
    let testEmail = 'xnkdjl@.......com'

    let result = generalUtilityFunc.validateEmail(testEmail);
    expect(result).toBeFalsy();
})

test('If invalid email - should return false', ()=> {
    let testEmail = 'xnkdjl@.com'

    let result = generalUtilityFunc.validateEmail(testEmail);
    expect(result).toBeFalsy();
})

test('If valid email - should return true', ()=> {
    let testEmail = 'hello_world@world.com'

    let result = generalUtilityFunc.validateEmail(testEmail);
    expect(result).toBeTruthy();
})

test('If valid email - should return true', ()=> {
    let testEmail = 'hello_worl324XLKJDJ_LKJSDFLKJSD@worldLSDIJF98980KJLN.com'

    let result = generalUtilityFunc.validateEmail(testEmail);
    expect(result).toBeTruthy();
})
const dotenv = require('dotenv');
dotenv.config();
const express = require('express');
const bodyParser = require('body-parser');
const massive = require('massive');
const cors = require('cors');
const app = express();
const session = require('express-session')
const middleware = require('./middlewares/middleware')

app.use(bodyParser.json());
app.use(cors());
app.use(express.static(`${__dirname}/../build`));

const authController = require('./controllers/authController')

const {
    SERVER_PORT,
    CONNECTION_STRING,
    SESSION_SECRET
} = process.env;

app.use(
    session({
        secret: SESSION_SECRET,
        resave: false,
        saveUninitialized: true
    })
)

app.use(middleware.checkSession)

massive(CONNECTION_STRING).then((dbInstance) => {
    app.set('db', dbInstance);
}).catch((err) => {
    console.log(`Error with Massive: ${err}`)
})

// ##### ENDPOINTS #####

// Auth Endpoints
app.get('/api/auth/me', authController.validate)
app.post('/api/auth/login', authController.login);
app.get('/api/auth/logout', authController.logout);
app.post('/api/auth/register', authController.register);

app.listen(SERVER_PORT, () => {
    console.log(`Creeping on Port: ${SERVER_PORT}`);
})
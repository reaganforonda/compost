const bcrypt = require('bcrypt');
const axios = require('axios');

module.exports = {
    register: async(req, res, next) => {
        const db = req.app.get('db');
        const{
            firstName, lastName, email, pw, phone, zip
        } = req.body;

        let lowerCaseEmail = email.toLowerCase();
        
        await db.GET_USERNAMES([lowerCaseEmail]).then(users => {
            if(users.length !==0) {
                res.status(400).send('Already Registered Under Email')
            } else {
                const salt = bcrypt.genSaltSync(10);
                const hash = bcrypt.hashSync(pw, salt);
                
                db.REGISTER_USER([firstName, lastName, lowerCaseEmail, hash, phone, zip]).then(user => {
                    res.status(200).send('User Created');
                }).catch((err) => {
                    console.log(`Error during Registration: ${err}`);
                    res.sendStatus(500);
                })
            }
        }).catch((err) => {
            console.log(`Error handling registration request: ${err}`)
            res.sendStatus(500);
        })
    },

    login: (req, res, next)=> {
        const db = req.app.get('db');
        const {email, pw} = req.body;

        let lowerCaseEmail = email.toLowerCase();

        db.GET_USERNAMES([lowerCaseEmail]).then(user=> {
            if(user.length ===0) {
                res.status(401).send('Please Register')
            } else {
                const validPassword = bcrypt.compareSync(pw, user[0].pw)
                const user_id = user[0].user_id

                if(validPassword){
                    req.session.user.user_id = user_id;
                    res.status(200).send('Login Complete')
                } else {
                    res.status(401).send('Incorrect Password')
                }
            }
        }).catch(err=>{
            console.log(`Error while getting users: ${err}`)
            res.sendStatus(500);
        })
    },
    
    logout: (req, res, next) => {
        req.session.destroy();
        res.status(200).send('Logout Compplete');
    },

    validate: (req, res, next) => {
        let user = req.session.user;
        if(req.session.user.user_id) {
            res.status(200).send(user);
        } else {
            res.status(401).send("Unautherized")
        }
    }
}